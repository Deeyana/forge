package fr.cesi.forge;

import java.io.Console;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;


/**
 * classe Hello: affiche un message avec messages de debug avant et apres.
 * 
 * Documentation complete.
 * 
 * <p>
 * ceci est un pararaphe.
 * <ul>
 * <li>puce 1</li>
 * </ul> 
 * 
 * @author 2089296
 *
 */
public class Hello {
	private static final Logger logger = 
			LoggerFactory.getLogger(Hello.class); 

	/**
	 * Programme principal.
	 * 
	 * @param args Aucun argument.
	 */
	public static void main (String[] args) {
		logger.debug("Avant le message"); 

		GereMessage gereMessage = new GereMessage(); 
		String test = gereMessage.gereMessage("msg_hello");
		
		System.out.println(test);
		
		logger.debug("Apres le message"); 
	}
}
