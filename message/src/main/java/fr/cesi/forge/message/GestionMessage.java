package fr.cesi.forge.message;

import java.io.Console;
import java.util.ResourceBundle;

public class GestionMessage {
	
	static private ResourceBundle labels = ResourceBundle.getBundle("label");
	
	static public String  getMessage(String key) {
		
		String retour = labels.getString(key);
	
		return(retour);
	}
	
}
